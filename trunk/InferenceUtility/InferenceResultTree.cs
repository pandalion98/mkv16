﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using mkv16;

namespace InferenceUtility
{
    public partial class InferenceResultTree : UserControl
    {
        private InferenceTree<Triple> results;

        public InferenceResultTree()
        {
            InitializeComponent();
        }

        protected void Redraw()
        {
            if (results == null)
                return;

            treeViewResults.Nodes.Clear();
            var treeRoot = CreateNodes(results);
            treeViewResults.Nodes.Add(treeRoot);

            treeRoot.Expand();
        }

        protected TreeNode CreateNodes(InferenceTree<Triple> branch)
        {
            if (branch.Value == null)
                return new TreeNode();

            var node = new TreeNode(branch.Value.Name);
            node.Tag = branch;

            foreach (var child in branch.Children)
            {
                node.Nodes.Add(CreateNodes(child));
            }

            return node;
        }

        protected void CreateListItems(InferenceTree<Triple> branch)
        {
            listViewItems.Clear();

            // Create columns
            listViewItems.Columns.Add("Name", 200);
            listViewItems.Columns.Add("Subject", 100);
            listViewItems.Columns.Add("Relation", 100);
            listViewItems.Columns.Add("Notion", 100);
            listViewItems.Columns.Add("Context", 200);
            listViewItems.Columns.Add("Source", 150);

            listViewItems.View = View.Details;

            // Create the list items
            ListViewItem listItem;
            Triple tripleItem;

            var displayCount = 0;
            var displayLimit = 100;

            foreach (var child in branch.Children)
            {
                tripleItem = child.Value;

                listItem = new ListViewItem(tripleItem.Name);
                listItem.SubItems.Add(tripleItem.Subject.Name);
                listItem.SubItems.Add(tripleItem.Relation.Name);
                listItem.SubItems.Add(tripleItem.Notion.Name);
                listItem.SubItems.Add(tripleItem.Source.Context);
                listItem.SubItems.Add(tripleItem.Source.Name);

                listViewItems.Items.Add(listItem);

                // Prevent entire collection being displayed
                displayCount++;
                if (displayCount >= displayLimit)
                    break;
            }
           
        }

        public InferenceTree<Triple> Results
        {
            get { return results; }
            set { results = value; Redraw(); }
        }

        private void action_viewBranch(object sender, TreeNodeMouseClickEventArgs e)
        {
            var node = e.Node;
            var branch = (InferenceTree<Triple>)e.Node.Tag;

            CreateListItems(branch);
        }
    }
}
