﻿using mkv16;

namespace InferenceUtility
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.resultsList = new System.Windows.Forms.ListView();
            this.labelFilter = new System.Windows.Forms.Label();
            this.textboxSearchField = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelNumItems = new System.Windows.Forms.Label();
            this.labelQuery = new System.Windows.Forms.Label();
            this.textBoxQuery = new System.Windows.Forms.TextBox();
            this.buttonQuery = new System.Windows.Forms.Button();
            this.openMemoryFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.workingLabelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.workingStatus = new System.Windows.Forms.ToolStripProgressBar();
            this.workingLabelProgress = new System.Windows.Forms.ToolStripStatusLabel();
            this.notionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // resultsList
            // 
            this.resultsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsList.Location = new System.Drawing.Point(0, 34);
            this.resultsList.Name = "resultsList";
            this.resultsList.Size = new System.Drawing.Size(742, 259);
            this.resultsList.TabIndex = 0;
            this.resultsList.UseCompatibleStateImageBehavior = false;
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Location = new System.Drawing.Point(54, 10);
            this.labelFilter.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(92, 13);
            this.labelFilter.TabIndex = 3;
            this.labelFilter.Text = "Filter starting point";
            // 
            // textboxSearchField
            // 
            this.textboxSearchField.Location = new System.Drawing.Point(152, 7);
            this.textboxSearchField.Name = "textboxSearchField";
            this.textboxSearchField.Size = new System.Drawing.Size(152, 20);
            this.textboxSearchField.TabIndex = 4;
            this.textboxSearchField.TextChanged += new System.EventHandler(this.action_SearchFilter);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.mainMenuStrip);
            this.flowLayoutPanel1.Controls.Add(this.labelFilter);
            this.flowLayoutPanel1.Controls.Add(this.textboxSearchField);
            this.flowLayoutPanel1.Controls.Add(this.labelNumItems);
            this.flowLayoutPanel1.Controls.Add(this.labelQuery);
            this.flowLayoutPanel1.Controls.Add(this.textBoxQuery);
            this.flowLayoutPanel1.Controls.Add(this.buttonQuery);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(742, 35);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.mainMenuStrip.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.mainMenuStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.mainMenuStrip.Location = new System.Drawing.Point(6, 6);
            this.mainMenuStrip.Margin = new System.Windows.Forms.Padding(2);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Padding = new System.Windows.Forms.Padding(6, 2, 2, 2);
            this.mainMenuStrip.Size = new System.Drawing.Size(43, 21);
            this.mainMenuStrip.TabIndex = 6;
            this.mainMenuStrip.Text = "mainMenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 17);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.openToolStripMenuItem.Text = "Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.action_Open);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.quitToolStripMenuItem.Text = "Exit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.action_Exit);
            // 
            // labelNumItems
            // 
            this.labelNumItems.AutoSize = true;
            this.labelNumItems.Location = new System.Drawing.Point(310, 10);
            this.labelNumItems.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelNumItems.Name = "labelNumItems";
            this.labelNumItems.Size = new System.Drawing.Size(45, 13);
            this.labelNumItems.TabIndex = 5;
            this.labelNumItems.Text = "All items";
            // 
            // labelQuery
            // 
            this.labelQuery.AutoSize = true;
            this.labelQuery.Location = new System.Drawing.Point(361, 10);
            this.labelQuery.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelQuery.Name = "labelQuery";
            this.labelQuery.Size = new System.Drawing.Size(43, 13);
            this.labelQuery.TabIndex = 8;
            this.labelQuery.Text = "Subject";
            // 
            // textBoxQuery
            // 
            this.textBoxQuery.Location = new System.Drawing.Point(410, 7);
            this.textBoxQuery.Name = "textBoxQuery";
            this.textBoxQuery.Size = new System.Drawing.Size(152, 20);
            this.textBoxQuery.TabIndex = 7;
            this.textBoxQuery.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxQuery_KeyPress);
            // 
            // buttonQuery
            // 
            this.buttonQuery.Location = new System.Drawing.Point(568, 7);
            this.buttonQuery.Name = "buttonQuery";
            this.buttonQuery.Size = new System.Drawing.Size(50, 20);
            this.buttonQuery.TabIndex = 9;
            this.buttonQuery.Text = "search";
            this.buttonQuery.UseVisualStyleBackColor = true;
            this.buttonQuery.Click += new System.EventHandler(this.buttonQuery_Click);
            // 
            // openMemoryFileDialog
            // 
            this.openMemoryFileDialog.FileName = "Memory.memory";
            this.openMemoryFileDialog.Filter = "Memory Store files|*.memory|All files|*.*";
            this.openMemoryFileDialog.Title = "Open memory file...";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.workingLabelStatus,
            this.workingStatus,
            this.workingLabelProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 294);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(742, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // workingLabelStatus
            // 
            this.workingLabelStatus.Name = "workingLabelStatus";
            this.workingLabelStatus.Size = new System.Drawing.Size(80, 17);
            this.workingLabelStatus.Text = "Working Status";
            // 
            // workingStatus
            // 
            this.workingStatus.Name = "workingStatus";
            this.workingStatus.Size = new System.Drawing.Size(150, 16);
            // 
            // workingLabelProgress
            // 
            this.workingLabelProgress.Name = "workingLabelProgress";
            this.workingLabelProgress.Size = new System.Drawing.Size(91, 17);
            this.workingLabelProgress.Text = "Working Progress";
            // 
            // notionBindingSource
            // 
            this.notionBindingSource.DataSource = typeof(mkv16.Notion);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 316);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.resultsList);
            this.Controls.Add(this.flowLayoutPanel1);
            this.MainMenuStrip = this.mainMenuStrip;
            this.MinimumSize = new System.Drawing.Size(430, 220);
            this.Name = "MainForm";
            this.Text = "Inference Utility";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView resultsList;
        private System.Windows.Forms.Label labelFilter;
        private System.Windows.Forms.TextBox textboxSearchField;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openMemoryFileDialog;
        private System.Windows.Forms.BindingSource notionBindingSource;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel workingLabelStatus;
        private System.Windows.Forms.ToolStripProgressBar workingStatus;
        private System.Windows.Forms.ToolStripStatusLabel workingLabelProgress;
        private System.Windows.Forms.Label labelNumItems;
        private System.Windows.Forms.Label labelQuery;
        private System.Windows.Forms.TextBox textBoxQuery;
        private System.Windows.Forms.Button buttonQuery;
    }
}

