﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using mkv16;

namespace InferenceUtility
{
    public partial class ResultsForm : Form
    {
        public ResultsForm()
        {
            InitializeComponent();
        }

        public InferenceTree<Triple> Results
        {
            get { return inferenceResultTree1.Results; }
            set { inferenceResultTree1.Results = value; }
        }
    }
}
