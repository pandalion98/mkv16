﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mkv16
{
    public interface IMemoryStore
    {
        void Save();
        void Save(string filename);

        void Load(string filename);
        void Purge();

        List<Notion> Notions { get; }
        List<Word> Words { get; }
        List<Triple> Triples { get; }
        List<Fact> Facts { get; }
        List<Source> Sources { get; }

        void AddNotion(Notion notion);
        void AddWord(Word notion);
        void AddTriples(Triple notion);
        void AddFact(Fact notion);
        void AddSource(Source source);

        IEnumerable<Notion> Match(string word);
    }
}
