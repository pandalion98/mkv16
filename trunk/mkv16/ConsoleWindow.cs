﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace mkv16
{
    public partial class ConsoleWindow : Form
    {
        public event EventHandler InputSent, OuputReceived;

        /// <summary>
        /// The last line inputted by the user
        /// </summary>
        public String lastInput;

        public ConsoleWindow()
        {
            InitializeComponent();
        }

        public void WriteLine(String text)
        {
            consoleBox.AppendText(text + "\n");

            if (OuputReceived != null)
                OuputReceived(this, new EventArgs());
        }

        private void handle_keyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == '\n')
            {
                lastInput = inputBox.Text;

                consoleBox.AppendText(lastInput);

                inputBox.Clear();

                if(InputSent != null)
                    InputSent(this, new EventArgs());
            }
        }

        public String LastInput
        {
            get { return lastInput; }
        }

        private void handle_windowFocus(object sender, EventArgs e)
        {
            inputBox.Focus();
        }
    }
}
