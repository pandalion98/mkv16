﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mkv16
{
    /// <summary>
    /// Encapsulating the common parts of a message, may be extended with more information about the domain and sender.
    /// </summary>
    public class Message
    {
        public string Domain;
        public string Sender;
        public string Text;

        public Message(string domain, string sender, string text)
        {
            Domain = domain;
            Sender = sender;
            Text = text;
        }

        public override string ToString()
        {
            return String.Format("Domain: {0}, Sender: {1}, Text: {2}", Domain, Sender, Text);
        }
    }
}
