﻿using System;
using System.Text;

namespace mkv16
{
    public delegate void MessageEvent(IProtocol context, Message message);
    public delegate void ConnectionEvent(IProtocol context, string report);

    public interface IProtocol
    {
        event MessageEvent MessageReceived, MessageSendSuccess, MessageSendFail;
        event ConnectionEvent ConnectionSuccess, ConnectionWarning, ConnectionFail;

        void Connect(string service, string name);
        void Disconnect();
        void Say(string domain, string recipient, string message);
        string ID { get; }
    }
}
