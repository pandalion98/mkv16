﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace mkv16
{
    class main
    {
        static void Main(string[] args)
        {
            try
            {
                var brain = new Brain();
                while (true)
                {
                    Thread.Sleep(1250);
                }
            }
            catch (Exception e)
            {
                // Show the exception, sleep for a while and try to establish a new connection to irc server
                Console.WriteLine(e.ToString());
                Thread.Sleep(5000);
                Main(args);
            }
        }
    }
}
