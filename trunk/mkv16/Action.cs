﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mkv16
{
    /// <summary>
    /// Encapsulates information about an action and the history of that action.
    /// </summary>
    public class Action
    {
        public string Name;
        private DateTime commandTimestamp;
        private DateTime eventTimestamp;
        public Dictionary<DateTime, string> CommandHistory;
        public Dictionary<DateTime, string> EventHistory;

        public Action(string name)
        {
            initialise(name, String.Empty);
        }

        public Action(string name, string command)
        {
            initialise(name, command);
        }

        private void initialise(string name, string command)
        {
            Name = name;
            CommandHistory = new Dictionary<DateTime, string>();
            EventHistory = new Dictionary<DateTime, string>();

            LogEvent("Created");
            LogCommand("");
        }

        public void LogEvent(string message)
        {
            eventTimestamp = DateTime.Now;
            EventHistory.Add(eventTimestamp, message);
        }

        public void LogCommand(string message)
        {
            commandTimestamp = DateTime.Now;
            CommandHistory.Add(commandTimestamp, message);
        }

        public string LastEvent
        {
            get { return EventHistory[eventTimestamp]; }
        }

        public string LastCommand
        {
            get { return CommandHistory[commandTimestamp]; }
        }

        public DateTime EventTimestamp
        {
            get { return eventTimestamp; }
        }

        public DateTime CommandTimestamp
        {
            get { return commandTimestamp; }
        }
    }
}
