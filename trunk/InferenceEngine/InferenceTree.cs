﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mkv16
{
    public class InferenceTree<T>
    {
        public InferenceTree()
        {
            Children = new List<InferenceTree<T>>();
        }

        public InferenceTree(T value)
        {
            Value = value;
            Children = new List<InferenceTree<T>>();
        }

        public virtual InferenceTree<T> Parent { get; set; }
        public List<InferenceTree<T>> Children { get; set; }
        public T Value { get; set; }

        public void AddChild(InferenceTree<T> child)
        {
            Children.Add(child);
            child.Parent = this;
        }

        public InferenceTree<T> AddChild(T child)
        {
            var item = new InferenceTree<T>(child);
            AddChild(item);
            return item;
        }
    }
}
