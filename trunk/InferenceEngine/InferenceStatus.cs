﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mkv16
{
    public enum Status
    {
        NotStarted, Started, InProgress, Error,Complete
    }

    public class InferenceStatus
    {
        public InferenceStatus()
        {
            CurrentStatus = Status.NotStarted;
            Message = String.Empty;
        }

        public Status CurrentStatus { get; set; }
        public String Message { get; set; }
        public Int64 Iterations { get; set; }
    }
}
