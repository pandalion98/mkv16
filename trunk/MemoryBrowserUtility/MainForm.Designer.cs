﻿using mkv16;

namespace MemoryBrowserUtility
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.resultsList = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.comboSearchCollection = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textboxSearchField = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelNumItems = new System.Windows.Forms.Label();
            this.openMemoryFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.notionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.workingLabelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.workingStatus = new System.Windows.Forms.ToolStripProgressBar();
            this.workingLabelProgress = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel1.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notionBindingSource)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultsList
            // 
            this.resultsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsList.Location = new System.Drawing.Point(0, 34);
            this.resultsList.Name = "resultsList";
            this.resultsList.Size = new System.Drawing.Size(622, 258);
            this.resultsList.TabIndex = 0;
            this.resultsList.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Type";
            // 
            // comboSearchCollection
            // 
            this.comboSearchCollection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboSearchCollection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboSearchCollection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSearchCollection.FormattingEnabled = true;
            this.comboSearchCollection.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.comboSearchCollection.Location = new System.Drawing.Point(91, 7);
            this.comboSearchCollection.Name = "comboSearchCollection";
            this.comboSearchCollection.Size = new System.Drawing.Size(135, 21);
            this.comboSearchCollection.TabIndex = 2;
            this.comboSearchCollection.SelectedValueChanged += new System.EventHandler(this.action_CollectionFilter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Search field";
            // 
            // textboxSearchField
            // 
            this.textboxSearchField.Location = new System.Drawing.Point(301, 7);
            this.textboxSearchField.Name = "textboxSearchField";
            this.textboxSearchField.Size = new System.Drawing.Size(152, 20);
            this.textboxSearchField.TabIndex = 4;
            this.textboxSearchField.TextChanged += new System.EventHandler(this.action_SearchFilter);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.mainMenuStrip);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.comboSearchCollection);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.textboxSearchField);
            this.flowLayoutPanel1.Controls.Add(this.labelNumItems);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(622, 35);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.mainMenuStrip.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.mainMenuStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.mainMenuStrip.Location = new System.Drawing.Point(6, 6);
            this.mainMenuStrip.Margin = new System.Windows.Forms.Padding(2);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Padding = new System.Windows.Forms.Padding(6, 2, 2, 2);
            this.mainMenuStrip.Size = new System.Drawing.Size(43, 21);
            this.mainMenuStrip.TabIndex = 6;
            this.mainMenuStrip.Text = "mainMenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 17);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.openToolStripMenuItem.Text = "Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.action_Open);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.quitToolStripMenuItem.Text = "Exit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.action_Exit);
            // 
            // labelNumItems
            // 
            this.labelNumItems.AutoSize = true;
            this.labelNumItems.Location = new System.Drawing.Point(459, 10);
            this.labelNumItems.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelNumItems.Name = "labelNumItems";
            this.labelNumItems.Size = new System.Drawing.Size(45, 13);
            this.labelNumItems.TabIndex = 5;
            this.labelNumItems.Text = "All items";
            // 
            // openMemoryFileDialog
            // 
            this.openMemoryFileDialog.FileName = "Memory.memory";
            this.openMemoryFileDialog.Filter = "Memory Store files|*.memory|All files|*.*";
            this.openMemoryFileDialog.Title = "Open memory file...";
            // 
            // notionBindingSource
            // 
            this.notionBindingSource.DataSource = typeof(Notion);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.workingLabelStatus,
            this.workingStatus,
            this.workingLabelProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 293);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(622, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // workingLabelStatus
            // 
            this.workingLabelStatus.Name = "workingLabelStatus";
            this.workingLabelStatus.Size = new System.Drawing.Size(80, 17);
            this.workingLabelStatus.Text = "Working Status";
            // 
            // workingStatus
            // 
            this.workingStatus.Name = "workingStatus";
            this.workingStatus.Size = new System.Drawing.Size(150, 16);
            // 
            // workingLabelProgress
            // 
            this.workingLabelProgress.Name = "workingLabelProgress";
            this.workingLabelProgress.Size = new System.Drawing.Size(91, 17);
            this.workingLabelProgress.Text = "Working Progress";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 315);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.resultsList);
            this.Controls.Add(this.flowLayoutPanel1);
            this.MainMenuStrip = this.mainMenuStrip;
            this.MinimumSize = new System.Drawing.Size(430, 220);
            this.Name = "MainForm";
            this.Text = "Memory Browser Utility";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notionBindingSource)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView resultsList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboSearchCollection;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textboxSearchField;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openMemoryFileDialog;
        private System.Windows.Forms.BindingSource notionBindingSource;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel workingLabelStatus;
        private System.Windows.Forms.ToolStripProgressBar workingStatus;
        private System.Windows.Forms.ToolStripStatusLabel workingLabelProgress;
        private System.Windows.Forms.Label labelNumItems;
    }
}

